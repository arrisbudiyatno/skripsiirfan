<?php
include "inc/koneksi.php";
if (isset($_POST['submit'])) {
	$id_member = $_POST['id_member'];
	$id_admin = "0";
	$tgl = date('Y-m-d H-i-s', strtotime($_POST['tanggal_berita']));
	$judul = mysql_real_escape_string($_POST['judul_berita']);
	$isi = mysql_real_escape_string($_POST['isi_berita']);
	$konfirmasi = "tidak";
	$tmp = $_FILES['foto_berita']['tmp_name'];
	$name = $_FILES['foto_berita']['name'];
	$size = $_FILES['foto_berita']['size'];
	$file_basename = substr($name, 0, strripos($name, '.')); // strip extention
	$file_ext = substr($name, strripos($name, '.')); // strip name
	$dir = "gambar/berita/";
		
	if ((!$judul)) {
		echo "<script language='javascript'>alert('Judul belum diisi'); self.history.back();</script>";
	} else {
		if (!empty($tmp)) {
			if ((($_FILES["foto_berita"]["type"] == "image/jpeg")
			|| ($_FILES["foto_berita"]["type"] == "image/jpg")
			|| ($_FILES["foto_berita"]["type"] == "image/png"))) {
				if($size <= 5000000) {
					$acak = rand(000000,999999);
					$newfile = $acak . $file_ext; 
					$gbr = $dir . $newfile; 					
					if (move_uploaded_file($tmp, $gbr)) { 
						$query = mysql_query("INSERT INTO berita (tanggal_berita,judul_berita,isi_berita,foto_berita,konfirmasi,id_member,id_admin) 
						VALUES
						('$tgl','$judul','$isi','$newfile','$konfirmasi','$id_member','$id_admin')");
						if ($query) {
							echo "<script language='javascript'>alert('Berhasil mengunggah berita'); document.location='berita_member.php'</script>";
						} else {
							echo "<script language='javascript'>alert('Gagal mengunggah berita'); self.history.back();</script>";
						}
					} else {
						echo "<script language='javascript'>alert('Foto gagal dipindah...'); self.history.back();</script>";
					}
				} else {
						echo "<script language='javascript'>alert('Ukuran file terlalu besar. Maksimal 5Mb...'); self.history.back();</script>";
				}
			} else {
				echo "<script language='javascript'>alert('Maaf tipe file salah, anda dapat mengunggah gambar dengan tipe jpeg,jpg dan png'); self.history.back();</script>";
			}
		} else {
			echo "<script language='javascript'>alert('Gambar masih kosong, silahkan ulangi lagi'); self.history.back();</script>";
		}
	}
} else {
	echo "<script language='javascript'>alert('Permintaan gagal dijalankan'); document.location='unggah_berita.php'</script>";
}
?>