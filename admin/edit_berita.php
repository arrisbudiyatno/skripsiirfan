<?php include "session.php"; ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Admin</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="asset/scss/bootstrap.min.css" rel="stylesheet">
    <link href="asset/css/font-awesome.min.css" rel="stylesheet">
    <link href="asset/css/prettyPhoto.css" rel="stylesheet">
    <link href="asset/css/price-range.css" rel="stylesheet">
    <link href="asset/css/animate.css" rel="stylesheet">
    <link href="asset/css/main.css" rel="stylesheet">
    <link href="asset/css/responsive.css" rel="stylesheet">     
 
</head>
<body>
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand-left" href="index.php">
                    <img src="assets/img/UI.jpg" width="200px" height="100px" />
                </a>

            </div>

        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav navbar-right">
                            <li><a href="index.php">Beranda</a></li>
                            <li><a href="member_masuk.php">Member Masuk</a></li>
                            <li><a href="berita_masuk.php">Berita Masuk</a></li>
                            <li><a class="menu-top-active" href="unggah_berita.php">Unggah Berita</a></li>
							<li><a href="data_pemain_pelatih.php">Data Pemain & Pelatih</a></li>
							<li><a href="jadwal_pertandingan_hasil.php">Jadwal Pertandingan</a></li>
                            <li><a href="logout.php">Logout</a></li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- MENU SECTION END-->

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Unggah Berita</h4>

                </div>
                        
                     <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             Silahkan Edit berita di sini :
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <form action="proses_unggah_berita.php" method="POST">
                                    <table width="100%" >
                                        <tbody>
                                        <?php
                                            include "../inc/koneksi.php";
                                            $id = mysql_query("SELECT id_admin FROM admin WHERE id_admin= '$_SESSION[id_admin]'");
                                            $hasil = mysql_fetch_array($id);
											
											$id_berita = $_GET['id_berita'];
											$berita = mysql_query("SELECT * FROM berita WHERE id_berita= '$id_berita'");
											$b = mysql_fetch_array($berita);
                                        ?>
                                            <tr>
                                                <td><input type="hidden" name="id_admin"value="<?php echo $hasil['id_admin']; ?>" readonly/></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" name="tanggal_berita"value="<?php echo date('d-m-Y H:i:s'); ?>" readonly/></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" name="judul_berita" value="<?php echo $b['judul_berita']; ?>" /></td>
                                            </tr>											
                                            <tr>
                                                <td>
                                                    <textarea id="editberita" rows="20" cols="65" name="isi_berita" value=""/><?php echo $b['isi_berita']; ?></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <input type="hidden" name="id_berita"  value="<?php echo $b['id_berita']; ?>">
                                                <td><button type="submit" name="submit" class="btn btn-primary">Simpan</button></td>
                                                <td><button type="reset" class="btn btn-success">Reset</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                    </div>
                    <!--End Advanced Tables -->
					
            </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="pull-right">Copyright © 2015 Designed by: IRFAN RISTIA RAHMADANI</span></p>
                </div>

            </div>
        </div>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
	
	<script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>

    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.scrollUp.min.js"></script>
    <script src="assets/js/price-range.js"></script>
    <script src="assets/js/jquery.prettyPhoto.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/tinymce/js/tinymce/tinymce.min.js"></script> <script type="text/javascript"></script>
    <script type="text/javascript">
        tinymce.init({
		selector: "#editberita",
		plugins: [
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });
        </script>
</body>
</html>
