<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Admin</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/style.css" rel="stylesheet" />
 
</head>
<body>
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand-left" href="index.html">
                    <img src="assets/img/UI.jpg" width="200px" height="100px" />
                </a>

            </div>

        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav navbar-right">
                            <li><a href="index.php">Beranda</a></li>
                            <li><a href="member_masuk.php">Member Masuk</a></li>
                            <li><a class="menu-top-active" href="berita_masuk.php">Berita Masuk</a></li>
                            <li><a href="unggah_berita.php">Unggah Berita</a></li>
                            <li><a href="data_pemain_pelatih.php">Data Pemain & Pelatih</a></li>
                            <li><a href="jadwal_pertandingan_hasil.php">Jadwal Pertandingan</a></li>
                            <li><a href="logout.php">Logout</a></li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- MENU SECTION END-->

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
            <?php
                    include "../inc/koneksi.php";
                    $id_berita = $_GET['id_berita'];
                    $ql = mysql_query ("SELECT * FROM berita WHERE id_berita='$id_berita'");
                    $b = mysql_fetch_array($ql);
                ?>
                <div class="col-md-12">
                    <h4 class="page-head-line"><?php echo $b['judul_berita']; ?></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php echo "<p class='text-content'><a href='../gambar/berita/$b[foto_berita]' target='_BLANK'><img src='../gambar/berita/$b[foto_berita]' width='500px'></a></p>*/
                    <p class='text-content'>$b[isi_berita]</p><br /><br />";
                    echo "<a href='berita_masuk.php'>Kembali</a>";
					echo "<a href='konfirmasi_berita.php?id_berita=$b[id_berita];' class='navbar-right btn btn-success'><i class='fa fa-lg fa-check'></i>Konfirmasi</a>";
					echo "<button class='navbar-right' onclick='window.print()'><i class='fa fa-lg fa-print'></i>&nbsp;Cetak</button>";
                    ?>
					
                    
                    <hr class="noscreen" />
                </div>
            </div>
        </div>
    </div>

    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="pull-right">Copyright © 2015 Designed by: IRFAN RISTIA RAHMADANI</span></p>
                </div>

            </div>
        </div>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
</body>
</html>
