<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Admin</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/js/jqueryCalendar/jqueryCalendar.css" rel="stylesheet">
 
</head>
<body>
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand-left" href="index.php">
                    <img src="assets/img/UI.jpg" width="200px" height="100px" />
                </a>

            </div>

        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav navbar-right">
                            <li><a href="index.php">Beranda</a></li>
                            <li><a href="member_masuk.php">Member Masuk</a></li>
                            <li><a href="berita_masuk.php">Berita Masuk</a></li>
                            <li><a href="unggah_berita.php">Unggah Berita</a></li>
							<li><a href="data_pemain_pelatih.php">Data Pemain & Pelatih</a></li>
							<li><a class="menu-top-active" href="jadwal_pertandingan_hasil.php">Jadwal Pertandingan</a></li>
                            <li><a href="logout.php">Logout</a></li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- MENU SECTION END-->

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Jadwal Pertandingan</h4>

                </div>
    
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Mengedit Jadwal Pertandingan
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="proses_edit_jadwal.php" method="post">
                                    <?php
                                    include "../inc/koneksi.php";
                                    $id_pertandingan = $_GET['id_pertandingan'];
                                    $edit_jadwal = mysql_query("SELECT * FROM pertandingan WHERE id_pertandingan = '$id_pertandingan'");
                                    $a = mysql_fetch_array($edit_jadwal);
                                    
                                    ?>
                                        <div class="form-group">
                                            <label>Tanggal Pertandingan</label>
                                            <input class="form-control" name="tanggal_pertandingan"  value="<?php echo $a['tanggal_pertandingan']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Waktu pertandingan</label>
                                            <input class="form-control" name="waktu_pertandingan" placeholder="Misalkan: 23:00 - 2:00 WIB" value="<?php echo $a['waktu_pertandingan']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Lawan Tanding</label>
                                            <input class="form-control" name="lawan_pertandingan" placeholder="Misalkan: Liverpool" value="<?php echo $a['lawan_pertandingan']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Hasil Pertandingan</label>
                                            <input class="form-control" name="hasil_pertandingan" placeholder="Misalkan: 2-0" value="<?php echo $a['hasil_pertandingan']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Pencetak Gol</label>
                                            <input class="form-control" name="pencetak_gol" placeholder="Misalkan: Javier Hernandez" value="<?php echo $a['pencetak_gol']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Main</label>
                                            <select name="main">
                                              <option value="kandang">Kandang</option>
                                              <option value="tandang">Tandang</option>
                                            </select>

                                        </div>
                                        <input type="hidden" name="id_pertandingan"  value="<?php echo $a['id_pertandingan']; ?>">
                                        <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                                        <button type="reset" class="btn btn-success">Reset</button>
                                    </form>
                                </div>                                
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
					
            </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="pull-right">Copyright © 2015 Designed by: IRFAN RISTIA RAHMADANI</span></p>
                </div>

            </div>
        </div>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
	
	<script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>
    <script src="assets/js/jqueryCalendar/jquery-1.6.2.min.js"></script>
    <script src="assets/js/jqueryCalendar/jquery-ui-1.8.15.custom.min.js"></script>

    <script>
        jQuery(function() {
            jQuery( "#inf_custom_someDateField" ).datepicker();
        });
    </script>
</body>
</html>
