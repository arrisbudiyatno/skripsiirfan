<?php include "session.php"; ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Admin</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/style.css" rel="stylesheet" />
 
</head>
<body>
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand-left" href="index.php">
                    <img src="assets/img/UI.jpg" width="200px" height="100px" />
                </a>

            </div>

        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav navbar-right">
                            <li><a href="index.php">Beranda</a></li>
                            <li><a href="member_masuk.php">Member Masuk</a></li>
                            <li><a class="menu-top-active" href="berita_masuk.php">Berita Masuk</a></li>
                            <li><a href="unggah_berita.php">Unggah Berita</a></li>
							<li><a href="data_pemain_pelatih.php">Data Pemain & Pelatih</a></li>
							<li><a href="jadwal_pertandingan_hasil.php">Jadwal Pertandingan</a></li>
                            <li><a href="logout.php">Logout</a></li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- MENU SECTION END-->

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Berita Masuk</h4>

                </div>
    
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             Daftar Berita Masuk :
                            <select class="btn btn-primary navbar-right" onchange="document.location.href=this.options[this.selectedIndex].value;" >
                            <option value="berita_masuk.php" selected />Semua Status Konfirmasi
                            <option value="berita_dikonfirmasi.php" />Sudah Dikonfirmasi
                            <option value="berita_tidak_dikonfirmasi.php" />Berita Tidak Dikonfirmasi
                            </select> 
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>ID_Member</th>
                                            <th>Tanggal Berita</th>
                                            <th>Judul Berita</th>
                                            <th>Isi Berita</th>                                            
											<th>Foto</th>
                                            <th>Konfirmasi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        include "../inc/koneksi.php";
                                        $daftar_berita = mysql_query("SELECT * FROM berita WHERE konfirmasi ='tidak' ORDER BY tanggal_berita DESC");
                                        while ($row = mysql_fetch_array($daftar_berita)) :
                                        ?>
                                        <tr class='odd gradeX'>
                                            <td align="center"><?php echo $row['id_member']; ?></td>
                                            <td align="center"><?php echo $row['tanggal_berita']; ?></td>
                                            <td><a href="baca_berita.php?id_berita=<?php echo $row['id_berita']; ?>"><?php echo substr(strip_tags($row['judul_berita']), 0, 20).' ... '; ?></td>
                                            <td><?php echo substr(strip_tags($row['isi_berita']), 0, 50).' ... '; ?></td>
                                            <td><?php echo $row['foto_berita']; ?></td>
                                            <td><?php echo $row['konfirmasi']; ?></td>

                                            <input type="hidden" name="id_berita"  value="<?php echo $h['id_berita']; ?>">
                                            <td><a href="konfirmasi_berita.php?id_berita=<?php echo $row['id_berita']; ?>" class="btn btn-success btn-sm"><i class="fa fa-lg fa-check"></i>Ubah</a>
                                            <a href="hapus_berita.php?id_berita=<?php echo $row['id_berita']; ?>" class="btn btn-danger btn-sm">Hapus</a></td>
                                        </tr>
                                        <?php endwhile; ?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
					
            </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="pull-right">Copyright © 2015 Designed by: IRFAN RISTIA RAHMADANI</span></p>
                </div>

            </div>
        </div>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
	
	<script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>
</body>
</html>
