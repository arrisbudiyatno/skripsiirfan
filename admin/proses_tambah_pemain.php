<?php
include "../inc/koneksi.php";
if (isset($_POST['submit'])) {
	$nama_pemain = $_POST['nama_pemain'];
	$kewarganegaraan_pemain = $_POST['kewarganegaraan_pemain'];
	$tempat_lahir_pemain = $_POST['tempat_lahir_pemain'];
	$tgl_lahir_pemain = $_POST['tgl_lahir_pemain'];
	$karir_sepakbola = $_POST['karir_sepakbola'];
	$level = $_POST['level_pemain'];
	$posisi = $_POST['posisi'];
	$kemunculan = $_POST['kemunculan'];
	$jumlah_gol = $_POST['jumlah_gol'];
	$tgl_bergabung = $_POST['tgl_bergabung'];
	$debut = $_POST['debut'];
	$tmp = $_FILES['foto_pemain']['tmp_name'];
	$name = $_FILES['foto_pemain']['name'];
	$size = $_FILES['foto_pemain']['size'];
	$file_basename = substr($name, 0, strripos($name, '.')); // strip extention
	$file_ext = substr($name, strripos($name, '.')); // strip name
	$dir = "../gambar/pemain/";

	if ((!$nama_pemain) || (!$kewarganegaraan_pemain) || (!$tempat_lahir_pemain) || (!$tgl_lahir_pemain) || (!$level) || (!$posisi) || (!$karir_sepakbola)){
				
		if(!$nama_pemain){
				echo "<script language='javascript'>alert('Nama pemain belum diisi'); self.history.back();</script>";
			}
		if(!$kewarganegaraan_pemain){
				echo "<script language='javascript'>alert('Kewarganegaraan pemain belum diisi'); self.history.back();</script>";
			}
		if(!$tempat_lahir_pemain){
				echo "<script language='javascript'>alert('Tempat lahir pemain belum diisi'); self.history.back();</script>";
			}
		if(!$tgl_lahir_pemain){
				echo "<script language='javascript'>alert('Tanggal lahir pemain belum diisi'); self.history.back();</script>";
			}
		if(!$karir_sepakbola){
				echo "<script language='javascript'>alert('Silahkan isi karir sepakbola pemain'); self.history.back();</script>";
			}
	}else {
		if (!empty($tmp)) {
			if ((($_FILES["foto_pemain"]["type"] == "image/jpeg")
			|| ($_FILES["foto_pemain"]["type"] == "image/jpg")
			|| ($_FILES["foto_pemain"]["type"] == "image/png"))) {
				if($size <= 5000000) {
					$acak = rand(000000,999999);
					$newfile = $acak . $file_ext; 
					$gbr = $dir . $newfile; 					
					if (move_uploaded_file($tmp, $gbr)) { 
						$query = mysql_query("INSERT INTO pemain (nama_pemain,kewarganegaraan_pemain,tempat_lahir_pemain,tgl_lahir_pemain,karir_sepakbola,foto_pemain,level_pemain,posisi,kemunculan,jumlah_gol,tgl_bergabung,debut) 
										VALUES
										('$nama_pemain','$kewarganegaraan_pemain','$tempat_lahir_pemain','$tgl_lahir_pemain','$karir_sepakbola','$newfile','$level','$posisi','$kemunculan','$jumlah_gol','$tgl_bergabung','$debut')");
						if ($query) {
							echo "<script language='javascript'>alert('Berhasil menambah pemain '); document.location='data_pemain_pelatih.php'</script>";
						} else {
							echo "<script language='javascript'>alert('Gagal menambah pemain'); self.history.back();</script>";
						}
					} else {
						echo "<script language='javascript'>alert('Foto gagal dipindah...'); self.history.back();</script>";
					}
				} else {
						echo "<script language='javascript'>alert('Ukuran file terlalu besar. Maksimal 5Mb...'); self.history.back();</script>";
				}
			} else {
				echo "<script language='javascript'>alert('Maaf tipe file salah, anda dapat mengunggah gambar resep dengan tipe jpeg,jpg dan png'); self.history.back();</script>";
			}
		} else {
			echo "<script language='javascript'>alert('Gambar masih kosong, silahkan ulangi lagi'); self.history.back();</script>";
		}
	}
} else {
	echo "<script language='javascript'>alert('Permintaan gagal dijalankan'); self.history.back();</script>";
}
?>