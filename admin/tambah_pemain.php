<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Admin</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/js/jqueryCalendar/jqueryCalendar.css" rel="stylesheet">
 
</head>
<body>
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand-left" href="index.php">
                    <img src="assets/img/UI.jpg" width="200px" height="100px" />
                </a>

            </div>

        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav navbar-right">
                            <li><a href="index.php">Beranda</a></li>
                            <li><a href="member_masuk.php">Member Masuk</a></li>
                            <li><a href="berita_masuk.php">Berita Masuk</a></li>
                            <li><a href="unggah_berita.php">Unggah Berita</a></li>
                            <li><a class="menu-top-active" href="data_pemain_pelatih.php">Data Pemain & Pelatih</a></li>
                            <li><a href="jadwal_pertandingan_hasil.php">Jadwal Pertandingan</a></li>
                            <li><a href="logout.php">Logout</a></li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- MENU SECTION END-->

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Data Pemain & Pelatih</h4>

                </div>
    
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="proses_tambah_pemain.php" method="post" enctype="multipart/form-data">
                                    
                                        <div class="form-group">
                                            <label>Level</label>
                                            <select name="level_pemain">
                                              <option value="pemain">Pemain</option>
                                              <option value="pelatih">Pelatih</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Pemain</label>
                                            <input class="form-control" name="nama_pemain" placeholder="Ketikan Nama Pemain disini...." >
                                        </div>
                                        <div class="form-group">
                                            <label>Kewarganegaraan</label>
                                            <input class="form-control" name="kewarganegaraan_pemain" placeholder="Ketikan kewarganegaraan disini...." >
                                        </div>
                                        <div class="form-group">
                                            <label>Tempat Lahir</label>
                                            <input class="form-control" name="tempat_lahir_pemain" placeholder="Ketikan tempat lahir disini...." >
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Lahir</label>
                                            <input class="form-control" name="tgl_lahir_pemain" placeholder="yyyy-mm-dd" >
                                        </div>
                                        <div class="form-group">
                                            <label>Posisi</label>
                                            <input class="form-control" name="posisi" placeholder="Misalkan : Goalkeeper" >
                                        </div>
                                        <div class="form-group">
                                            <label>Jumlah Tampil</label>
                                            <input class="form-control" name="kemunculan" placeholder="Misalkan : 175" >
                                        </div>
                                        <div class="form-group">
                                            <label>Jumlah Gol</label>
                                            <input class="form-control" name="jumlah_gol" placeholder="Misalkan : 50" >
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Bergabung</label>
                                            <input class="form-control" name="tgl_bergabung" placeholder="yyyy-mm-dd">
                                        </div>
                                        <div class="form-group">
                                            <label>Debut</label>
                                            <input class="form-control" name="debut" placeholder="Misalkan : 7 Aug 2011 v Man City (N)">
                                        </div>
                                        <div class="form-group">
                                            <label>Karir Sepakbola</label>
                                            <textarea rows="10" cols="80" name="karir_sepakbola" placeholder="Ketikan karir sepakbola pemain disini..."/></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Foto</label>
                                            <input type="file" name="foto_pemain" accept='image/*'>
                                        </div>
                                        
                                        <button type="submit" name="submit" class="btn btn-primary">Tambah</button>
                                        <button type="reset" class="btn btn-success">Reset</button>
                                    </form>
                                </div>                                
                            </div>
                        </div>
                    </div>
                     <!-- End Form Elements -->
                    
            </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="pull-right">Copyright © 2015 Designed by: IRFAN RISTIA RAHMADANI</span></p>
                </div>

            </div>
        </div>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
    
    <script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>
    <script src="assets/js/jqueryCalendar/jquery-1.6.2.min.js"></script>
    <script src="assets/js/jqueryCalendar/jquery-ui-1.8.15.custom.min.js"></script>

    <script>
        jQuery(function() {
            jQuery( "#inf_custom_someDateField" ).datepicker();
        });
    </script>
</body>
</html>
