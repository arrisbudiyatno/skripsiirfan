<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Portal Berita Manchaster United</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">     
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>

<body>
	<header id="header">
		<div class="navbar navbar-inverse set-radius-zero">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="logo pull-left"><a href="index.php"><img src="gambar/images/home/UI.png" width="200px" height="100px" alt="" /></a></div>
					</div>
					<div class="col-sm-9">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<div class="col-sm-3">
									
								</div>								
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Silahkan masuk ke akun anda</h2>
						<form action="proses_login_member.php" method="POST">
							<input type="text" name="user_name" placeholder="Username" />
							<input type="password" name="password" placeholder="Kata Sandi" />
							<button type="submit" class="btn btn-default">Masuk</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">Atau</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Silahkan Mendaftar!</h2>
						<form action="member.php" method="POST">
							<input type="text" name='nama_member' placeholder="Nama"/>
							<input type="text" name='user_name' placeholder="Username"/>
							<input type="email" name='email_member' placeholder="Alamat Email"/>
                                           <label class='radio-inline'>											
                                                <input type='radio' name='jk_member' value='1' checked=''>Laki-laki
                                            </label>
                                            <label class='radio-inline'>
                                                <input type='radio' name='jk_member' value='2'>Perempuan
                                            </label>
							<input type="password" name='password1' placeholder="Kata Sandi"/>
							<input type="password" name='password2' placeholder="Ulangi Kata Sandi"/>
							<button type="submit" name="submit" class="btn btn-default">Daftar</button>
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	<footer id="footer"><!--Footer-->
		<div class="footer-widget" class="center">
			<div class="container">
				<div class="row">
					<div class="col-sm-15">
						<div class="single-widget">
						<center>
							<h2>KONTAK ADMIN</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Telpon 	: 08994355146</a></li>
								<li><a href="#">Email	: irfan931226@gmail.com</a></li>
								<li><a href="#">BBM		: 54F0EA54</a></li>
							</ul>
							</center>
						</div>
					</div>                  					
				</div>
			</div>
		</div>		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">				
					<marquee><p class="pull-left">Designed by: IRFAN RISTIA RAHMADANI</p></marquee>
				</div>
			</div>
		</div>
	</footer>  
	</footer><!--/Footer-->
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>