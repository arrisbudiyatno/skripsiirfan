<?php
	include "inc/koneksi.php";
	include "session.php";
	error_reporting(0);
	?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Portal Berita Manchaster United</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">     
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>
	<header id="header">
		<div class="navbar navbar-inverse set-radius-zero">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="logo pull-left"><a href="index.php"><img src="gambar/images/home/UI.png" width="200px" height="100px" alt="" /></a></div>
					</div>
					<div class="col-sm-9">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<div class="col-sm-3">
									<div class="search_box pull-right">
										<form action="pencarian.php" method="get">
											<input type="text" name="cari" placeholder="Pencarian"/>
										</form>
									</div>
								</div>								
								<li><a href="logout.php"><i class="fa fa-lock fa-1x"></i><font color="#fff">Logout</font></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section id="slider">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
						</ol>						
						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-6">
									<h1><span>Manchester</span>United</h1>
								  	<p>Portal Berita Manchester United</p>									
								</div>
								<div class="col-sm-6"><img src="gambar/images/home/UI.png" class="girl img-responsive" alt="" /></div>
						  </div>
							<div class="item">
								<div class="col-sm-6">
									<h1><span>Manchester</span>United</h1>
								  	<p>Portal Berita Manchester United</p>	
								</div>
								<div class="col-sm-6">
									<img src="gambar/images/home/UI.png" class="girl img-responsive" alt="" />
								</div>
							</div>							
						</div>						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<a href="member/index.php"><h2>Beranda</h2></a>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="unggah_berita.php">Unggah Berita</a></h4>
										</div>
									</div>
								</div>
							</div> 
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="member/profil_pemain.php">Profil Pelatih & Pemain</a></h4>
										</div>
									</div>
								</div>
							</div>							
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="member/sejarah_manchaster_united.php">Sejarah Manchaster United</a></h4>
										</div>
									</div>
								</div>
							</div> 
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="member/stadion.php">Stadion</a></h4>
										</div>
									</div>
								</div>
							</div> 
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="member/jadwal_pertandingan.php">Jadwal Pertandingan</a></h4>
										</div>
									</div>
								</div>
							</div> 
						</div><!--/category-products-->	
						<a href="member/index.php"><h2>Arsip Berita</h2></a>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<?php
							$query = "SELECT DISTINCT date_format(tanggal_berita, '%M %Y') as bulantahun FROM berita";
							$hasil = mysql_query($query);
							echo "<ul>";
							while ($data = mysql_fetch_array($hasil))
							{
							    echo "<li><a href='viewarsip.php?blnth=".$data['bulantahun']."'>".$data['bulantahun']."</a></li>";
							}
							echo "</ul>";

							?>
						</div>
					</div>
				</div>
			<div class="category-tab"><!--category-tab-->
				<div class="col-sm-12">
					<ul class="nav nav-tabs">
						<li class="active"><a href="unggah_berita.php">Unggah Berita</a></li>
						<li><a href="berita_member.php">Baca Berita</a></li>
						<li><a href="chat.php">Chat Group</a></li>
						<li><a href="profile.php">Profile</a></li>
						<!--<li><a href="logout.php">Logout</a></li>-->
					</ul>
				</div>

				<div class="panel panel-default">
				<div class="panel-body">
                            <div class="table-responsive">
			<form role="form" action="proses_unggah_berita.php" method="POST" enctype="multipart/form-data">
				<table width="100%" >
					<tbody>
					<?php
						include "inc/koneksi.php";
						$member = mysql_query("SELECT id_member FROM member WHERE id_member= '$_SESSION[id_member]'");
						$hasil = mysql_fetch_array($member);
					?>
						<tr>
							<td><input type="hidden" name="id_member"value="<?php echo $hasil['id_member']; ?>" readonly/></td>
						</tr>
						<tr>
							<td><input type="text" name="tanggal_berita"value="<?php echo date('d-m-Y H:i:s'); ?>" readonly/></td>
						</tr>
						<tr>
							<td><input type="text" name="judul_berita" placeholder="Masukkan judul disini" /></td>
						</tr>
						<tr>
							<td>Unggah Gambar berita disini : <input type="file" name="foto_berita" accept='image/*'></td>
						</tr>
						<tr>
							<td>
								<textarea rows="20" cols="65" name="isi_berita" /></textarea>
							</td>
						</tr>
						<tr>
							<td><button type="submit" name="submit" class="btn btn-primary">Unggah</button></td>
						</tr>
					</tbody>
				</table>
				</form>
				</div>
				</div>
				</div>
			</div><!--/category-tab-->
						
					</div>
				</div>
			</div>
		</div>
	</section>	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget" class="center">
			<div class="container">
				<div class="row">
					<div class="col-sm-15">
						<div class="single-widget">
						<center>
							<h2>KONTAK ADMIN</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Telpon 	: 08994355146</a></li>
								<li><a href="#">Email	: irfan931226@gmail.com</a></li>
								<li><a href="#">BBM		: 54F0EA54</a></li>
							</ul>
							</center>
						</div>
					</div>                  					
				</div>
			</div>
		</div>		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">				
					<marquee><p class="pull-left">Designed by: IRFAN RISTIA RAHMADANI</p></marquee>
				</div>
			</div>
		</div>
	</footer>  
	</footer><!--/Footer-->
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
	<script src="js/tinymce/js/tinymce/tinymce.min.js"></script> <script type="text/javascript"></script>
	<script type="text/javascript">
		tinymce.init({
			selector: "textarea",
			plugins: [
				"searchreplace visualblocks code fullscreen",
				"insertdatetime media contextmenu paste"
			],
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
		});
		</script>

</body>
</html>