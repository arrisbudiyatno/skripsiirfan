-- phpMyAdmin SQL Dump
-- version 3.1.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 05, 2015 at 12:52 PM
-- Server version: 5.1.33
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `berita_mu`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` int(5) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `user_name`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
  `id_member` int(5) NOT NULL,
  `id_admin` int(5) NOT NULL,
  `id_berita` int(5) NOT NULL AUTO_INCREMENT,
  `judul_berita` varchar(250) NOT NULL,
  `tanggal_berita` datetime NOT NULL,
  `isi_berita` text NOT NULL,
  `konfirmasi` enum('ya','tidak') DEFAULT 'tidak',
  `foto_berita` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_berita`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_member`, `id_admin`, `id_berita`, `judul_berita`, `tanggal_berita`, `isi_berita`, `konfirmasi`, `foto_berita`) VALUES
(0, 1, 37, 'Schneiderlin Tak Mau Lewatkan Kesempatan Dilatih Van Gaal', '2015-07-14 15:19:27', '<p><strong>Manchester</strong> - Faktor Louis van Gaal sangat berpengaruh dalam keputusan Morgan Schneiderlin untuk menerima tawaran Manchester United. Schneiderlin tak mau melewatkan kesempatan dilatih oleh figur yang disebutnya sebagai salah satu manajer terbaik di dunia itu.<br /><br />Setelah tujuh tahun membela Southampton, Schneiderlin akhirnya berganti klub. Gelandang asal Prancis itu pindah ke MU dengan nilai transfer yang kabarnya mencapai 24 juta poundsterling.<br /><br />Selama berseragam Southampton, Schneiderlin pernah ditangani oleh manajer-manajer seperti Alan Pardew, Nigel Adkins, Mauricio Pochettino, dan Ronald Koeman. Dia yakin kemampuannya akan makin meningkat jika diasah oleh seorang Van Gaal.<br /><br />Dia adalah seorang manajer besar yang sudah melatih tim-tim terbaik di dunia dan ketika saya bicara dengannya, dia menginginkan hal besar untuk Manchester United dan tim ini, ucap Schneiderlin kepada<em> MUTV</em>.<br /><br />Dia membuat saya mengerti betapa besarnya klub ini dan pentingnya melakukan hal yang tepat dan main bagus. Jadi, bagi saya itu merupakan bagian besar dan saya tak sabar berlatih bersamanya dan mengalami kemajuan karena saya masih muda dan saya tahu saya masih bisa meningkat di sini, imbuhnya.<br /><br />Saya bukanlah seorang pemain jadi. Setiap tahun saya merasa mengalami kemajuan, saya merasa telah belajar dari setiap manajer di Southampton, yang mana merupakan waktu yang luar biasa bagi saya, tutur pemain berusia 25 tahun itu.<br /><br />Saya ucapkan terima kasih untuk semua orang-orang di Southampton karena mereka telah membantu saya untuk melewati banyak hal dan membantu saja untuk maju sebagai seorang pemain, lanjut Schneiderlin.<br /><br />Namun, sekarang saya tahu bahwa saya akan lebih baik lagi karena saya akan bermain bersama pemain-pemain yang lebih baik dan salah satu manajer terbaik di dunia,\\" kata dia.<br /><br />Schneiderlin adalah salah satu dari empat pemain baru yang telah direkrut MU di bursa transfer musim panas ini selain Bastian Schweinsteiger, Memphis Depay, dan Matteo Darmian. Dia akan mengikuti tur pramusim bersama timnya di Amerika Serikat.</p>', 'ya', '295654.jpg'),
(0, 1, 38, 'Schweinsteiger di Posisi Lima Daftar Pemain (Veteran) Termahal', '2015-07-14 15:56:52', '<p>Keputusan Manchester United memboyong Bastian Schweinsteiger mengundang beberapa pertanyaan karena dia sering cedera dan sudah tak lagi muda. Schweinsteiger juga menjadi pemain veteran termahal <em>The Red Devils</em>.<br /><br />MU harus mengeluarkan 20 juta euro untuk mengangkut Schweinsteiger dari Allianz Arena ke Old Trafford. Kedatangan gelandang internasional Jerman itu melanjutkan aktivitas belanja Setan Merah yang sangat aktif di musim panas ini.<br /><br />Namun pembelian Schweinsteiger juga disertai beberapa pertanyaan terkait cedera yang mulai sering mendatangi plus usianya yang bakal menginjak 31 tahun pada 1 Agustus mendatang. Dalam empat musim terakhir dia tak pernah bermain lebih dari 30 kali per musim untuk <em>Die Roten</em>, termasuk musim lalu di mana dia bermain 28 kali di Bundesliga.<br /><br />Apalagi Schweinsteiger ditebus dengan harga yang tidak bisa dibilang murah untuk pemain seusianya.<br /><br />Menurut catatan <em>Infostrada</em>, Schweinsteiger menjadi pemain veteran termahal kelima sepanjang sejarah. Yang dimaksud veteran di sini adalah mereka yang sudah berusia lebih dari 30 tahun.<br /><br />Schweinsteiger bersanding dengan Claude Makelele, yang dibeli Chelsea dari Real Madrid dengan harga 20 juta euro ketika berusia 30 tahun. Keduanya kini menjadi pemain veteran termahal di Premier League.<br /><br />Memuncaki daftar pemain veteran termahal adalah Gabriel Batistuta. AS Roma membelinya dengan harga 32,5 juta euro dari Fiorentina pada tahun 2000.</p>', 'ya', '835540.jpg'),
(0, 1, 39, 'Schneiderlin Siap Hadapi Tekanan di MU', '2015-07-14 16:02:41', '<p><strong>Manchester</strong> - Morgan Schneiderlin menyadari bahwa pindah ke Manchester United akan membuatnya merasakan tekanan yang lebih besar. Namun, dia mengaku sudah siap untuk menghadapinya.<br /><br />Schneiderlin sebelumnya tak punya pengalaman membela klub besar. Di negara asalnya, Prancis, dia hanya memperkuat Strasbourg (1995-2008). Gelandang kelahiran 8 November 1989 itu kemudian hijrah ke Southampton dan membela<em> The Saints </em>selama tujuh musim.<br /><br />Schneiderlin akan segera tahu bagaimana rasanya bermain untuk klub besar setelah dibeli MU seharga 24 juta poundsterling. Dia dikontrak (Setan Merah) selama empat tahun dan menjadi salah satu wajah baru di Old Trafford selain Memphis Depay, Matteo Darmian, dan Bastian Schweinsteiger.<br /><br />Schneiderlin, juga Schweinsteiger, diharapkan bisa mengurangi ketergantungan MU kepada Michael Carrick di lini tengah. Dia mengaku siap menjawab tantangan tersebut dan tak sabar untuk bermain di depan publik Old Trafford.<br /><br />Ini adalah mimpi yang jadi nyata. Ini adalah klub terbesar di dunia, tutur Schneiderlin kepada <em>MUTV.</em><br /><br />Saya berusaha sepanjang hidup saya untuk hal ini dan hari ini saya sangat bahagia dan ini adalah hari yang membanggakan. Ini merupakan awal dari hal baru untuk saya, sebuah petualangan baru, sebuah langkah baru, dan lebih menuntut, dengan tekanan yang lebih besar. Tapi, saya siap untuk itu dan tak sabar untuk mulai bermain, tambahnya.<br /><br />Saya ingat pertama kali saya tiba di stadion (Old Trafford).Saya langsung mengatakan bahwa ini adalah stadion terbaik karena atmosfernya, karena konteksnya, karena namanya. Saya sangat menantikan bermain setiap pekan di hadapan ribuan suporter dan saya tak sabar untuk memulainya, lanjut Schneiderlin. <br /><br />Musim lalu saya sukses bersama Southampton. Namun, semoga sekarang saya akan memenangi setiap laga bersama Manchester United, katanya.</p>', 'ya', '228057.jpg'),
(0, 1, 35, 'Agen: Rafael Tak Masuk Rencana Van Gaal', '2015-07-14 15:11:59', '<p><strong>Manchester</strong> - Rafael da Silva tampaknya kian dekat dengan pintu keluar dari Manchester United. Agen pemain asal Brasil itu, Cassiano Pereira, menyebut Rafael tak ada di dalam rencana Louis van Gaal.<br /><br />Bersama <em>The Red Devils</em> musim lalu, Rafael tak menjadi pilihan utama. Dia cuma 10 kali bermain di Liga Inggris, enam di antaranya menjadi starter.<br /><br />Dengan jumlah bermain yang sedikit, maka tak mengejutkan bahwa Rafael disebut tak berada dalam rencana jangka panjang Van Gaal. Apalagi, kontraknya di MU tinggal setahun lagi serta tak dibawa dalam tur MU ke Amerika Serikat.<br /><br />Kontraknya dengan Manchester United akan berakhir pada bulan Juni 2016 dan Louis van Gaal sudah mengatakan bahwa dia bukan bagian dari rencana. Jadi, sang pemain ingin pergi dan kami berbincang dengan lebih dari satu klub, kata Cassiano di <em>Tuttomercato</em>.<br /><br />Soal klub baru untuk Rafael, Cassiano sudah menyebut satu nama. AS Roma dinilai menjadi klub yang pas sebagai tempat bemain baru untuk bek 25 tahun itu.<br /><br />Roma akan menjadi lebih dari pilihan bagus untuk dia dan kami sangat senang bisa bernegosiasi dengan klub, ucap Cassiano.<br /><br />Italia akan menjadi kesempatan besar, terutama mempertimbangkan bahwa dia mempunyai paspor Portugis. Bagaimanapun juga, saya mesti mengungkapkan kalau saya belum mendengar ketertarikan pada Rafael dari klub Italia lainnya, imbuhnya.</p>', 'ya', '791320.jpg'),
(0, 1, 36, 'Ferdinand Ragu Madrid Mau Lepas Ramos ke Manchester United', '2015-07-14 15:16:33', '<p><strong>London</strong> - Bekas pemain Manchester United Rio Ferdinand angkat bicara soal rumor Sergio Ramos. Menurut Ferdinand, Real Madrid tidak akan mau melego salah satu pemain terbaiknya itu.<br /><br />Saga transfer Ramos menjadi salah satu pembicaraan utama di bursa musim panas 2015. Bek internasional Spanyol itu kencang diisukan ingin meninggalkan Madrid karena tidak senang dengan pemecatan Carlo Ancelotti dan tidak puas dengan gajinya yang bahkan jauh di bawah Toni Kroos dan Karim Benzema. <br /><br />MU disebut-sebut berhasrat mendapatkan jasa pesepakbola berusia 29 tahun itu. Ramos sendiri, kabarnya, telah mengatakan bahwa dirinya ingin bergabung MU meskipun juga diminati Manchester City. <br /><br />Setan Merah dikabarkan suda menawarkan 28,5 juta pound untuk memboyong Ramos ke Old Trafford. Tapi ada yang menyebut, MU bersedia membayar 40 juta pound dan masih harus menyerahkan David de Gea.<br /><br />Ferdinand merasa isu Ramos hanyalah akal-akalan dalam usaha untuk menaikkan bayarannya di Madrid. Apalagi, selama 10 tahun di klub Ramos telah menjadi bagian yang sangat penting untuk skuat <em>El Real.</em></p>\r\n<p>Sergio Ramos ke Manchester City? Dia akan ke Manchester United pada hari lalu. Sekarang dia akan pindah ke Manchester City, agen dia baik-baik saja malah sudah bekerja dengan bagus. Tim manajemen dia sudah menempatkan dia di antara dua klub yang memiliki kekuatan finansial terbesar, cetus Ferdinand yang dilansir <em>Football Espana.</em><br /><br />Aku pikir tidak akan begitu. Aku pikir dia akan tetap bertahan di Madrid. Dia akan bermain bersama (Raphael) Varane atau Pepe di musim depan. Mereka memiliki tiga bek terbaik di sana. Mereka bisa saja melepas Ramos kecuali mendapatkan Varane lain. Aku kira mereka tidak akan membiarkan dia pergi.<br /><br />Itu adalah uang yang terlalu banyak untuk menebus seorang pesepakbola berusia 29 tahun tanpa memiliki nilai jual kembali. Jadi itu adalah kesepakatan yang begitu besar. Tapi mengapa dia ingin pergi ke Manchester City sebelum ke Manchester United? Aku tidak bisa memahaminya.</p>', 'ya', '724456.jpg'),
(13, 0, 25, 'Schneiderlin Dianggap sebagai Pengganti Ideal untuk Carrick', '2015-07-02 01:27:02', '<p><strong>London</strong> - Morgan Schneiderlin disebut-sebut sebagai salah satu buruan utama Manchester United di bursa transfer musim panas ini. Rio Ferdinand menilai Schneiderlin adalah pemain yang layak dikejar oleh MU.<br /><br />MU terlalu bergantung pada Michael Carrick pada musim lalu. Jika Carrick absen, <em>The Red Devils </em>tak punya pengganti yang sepadan di posisi <em>holding midfielder.</em> Daley Blind memang bisa dipasang di posisi tersebut, tapi dia belum mampu menjalankan peran sama baiknya dengan Carrick.<br /><br />Yang jadi masalah, Carrick rentan cedera. Gelandang yang kini berusia 33 tahun itu tercatat empat kali cedera sepanjang musim 2014/2015 dan harus melewatkan 21 pertandingan.<br /><br />Jika MU berhasil memboyong Schneiderlin dari Southampton, Ferdinand yakin permasalahan di atas akan bisa teratasi. Menurutnya, Schneiderlin adalah pengganti yang ideal untuk Carrick.<br /><br />20 juta sekian poundsterling, tampaknya itu akan dituntaskan. Dia adalah seorang pemain yang bisa menambahkan sesuatu ke United, seorang pemain yang tanggguh, bisa mengoper bola dengan baik, dan mengalirkannya dari belakang ke depan, ujar Ferdinand di channel <em>Youtube</em>-nya.<br /><br />Saya pikir dia adalah pemain bagus, tambahan yang bagus untuk skuat, khususnya jika seseorang seperti Carrick, yang sangat berpengaruh untuk United, cedera, lanjutnya.<br /><br />Saya pikir pemain ini bisa menggantikan. Saya pikir dia adalah seorang pemain yang sangat bagus dan bertalenta, kata mantan bek MU yang pensiun pada akhir musim lalu itu.</p>', 'ya', '140350.jpg'),
(13, 0, 26, 'Rojo: Di Maria Akan Segera Tampil Oke di MU', '2015-07-02 01:34:31', '<p><strong></strong>Angel Di Maria tampil kurang optimal di musim pertamanya bersama Manchester United. Penggawa tim nasional Argentina itu diyakini oleh Marcos Rojo akan segera tampil oke bersama Setan Merah.<br /><br />Rojo dan Di Maria sama-sama pindah ke Old Trafford mulai musim panas tahun lalu. Rojo didatangkan dari Sporting Lisbon, sementara Di Maria didatangkan dari Real Madrid.<br /><br />Di musim pertama bersama MU, Rojo mengalami banyak gangguan cedera. Alhasil, dia cuma bermain sebanyak 25 kali di semua ajang bersama MU musim 2014/2015.<br /><br />Sementara itu, Di Maria bermain sebanyak 32 kali dengan sumbangan empat gol dan 12 <em>assist</em> di semua ajang.<br /><br />Penampilan itu tak mencerminkan performa Di Maria semasa memperkuat Madrid semusim sebelumnya. Dia tercatat mencatatkan 11 gol dan 26 <em>assist</em> saat 52 kali membela Madrid di semua ajang. Penampilan itu yang meyakinkan MU mau membayar uang sebesar sekitar 59 juta poundsterling untuk <em>El Real</em>.<br /><br />Hambatan dalam proses adaptasi dinilai membuat Di Maria tak bisa tampil maksimal. Oleh karena itu, Di Maria disebut Rojo akan tampil maksimal dalam beberapa bulan ke depan.<br /><br />Untuk saya, pemain paling bertalenta di Manchester United adalah Angel. Ada pemain-pemain hebat di klub, tapi buat saya Angel merupakan yang terbaik di tim dan di dunia. Tahun lalu sulit baginya. Dia mempunyai masalah adaptasi, tapi itu normal, kata Rojo di <em>Sky Sport</em>.<br /><br />Pemain lain datang ke negara baru dan bahasa baru, harus beradaptasi pada perbedaan dan itulah apa yang terjadi. Tapi, saya yakin dia akan bermain bagus dalam bulan-bulan ke depan, imbuhnya.</p>', 'ya', '935729.jpg'),
(11, 0, 44, 'Mata & Memphis Cetak Gol, MU Kalahkan San Jose 3-1', '2015-07-22 18:29:07', '<p>Manchester United meraih kemenangan keduanya dalam tur pramusim di Amerika Serikat. Mempis Depay dan Juan Mata menyumbang gol untuk membawa MU mengalahkan San Jose Earquakes 3-1, Rabu (22/7/2015).<br /><br />Pertandingan antara San Jose melawan MU digelar di Stadion Avaya. Di laga pramusim pertama melawan Club America, \\''Setan Merah\\'' menang tipis 1-0 lewat gol tunggal Morgan Scheneiderlin.<br /><br />Menghadapi San Jose, manajer MU Louis van Gaal menurunkan tim yang sama saat menghadapi Club America. Memphis Depay membuka peluang bagi MU di menit keempat dengan sepakan kencang yang mengarah ke kiper David Bingham. Namun, Wayne Rooney maupun Juan Mata gagal memaksimalkannya.<br /><br />Setelah beberapa peluang gagal menghasilkan gol, MU pun akhirnya berhasil memecah kebuntuan di menit ke-32. Diawali dari umpan tarik Young kepada Mata, bola diteruskan dengan sepakan ke tiang jauh. <br /><br />Sempat terjadi perdebatan tentang siapa pencetak gol pertama MU lantaran Memphis terlihat merentangkan kakinya ketika menyongsong sepakan Mata. Tapi pada akhirnya Mata disahkan menjadi pencetak gol itu.<br /><br />Berselang empat menit, MU menggandakan skor menjadi 2-0. Memphis, pemain yang digaet dari PSV Eindhoven, mencatatkan namanya di papan skor. Memphis dengan gesit memotong bola backpass yang diceploskan ke gawang San Jose.<br /><br />Tiga menit sebelum turun minum, tim tuan rumah memperkecil ketinggalan menjadi 1-2. Shea Salinas melewati penjagaan Morgan Schneiderlin untuk mengoper kepada Fatai Alashe, yang diteruskan dengan tembakan tanpa bisa dibendung kiper Sam Johnstone.</p>\r\n<p>Beberapa saat sebelum paruh pertama selesai, MU menciptakan peluang lain. Namun, tembakan Young dari jarak dekat bisa diblok Bingham.<br /><br />Di paruh kedua, Van Gaal mengeluarkan hampir semua pemainnya kecuali Johnstone. Paddy McNair, Chris Smalling, Jonny Evans, Tyler Blackett, Jesse Lingard, Bastian Schweinsteiger, Ander Herrera, Andreas Perreira, Adnan Januzaj, dan James Wilson masuk.<br /><br />Kombinasi antara pemain-pemain muda MU menghasilkan gol ketiga di menit ke-61. Operan Januzaj kepada Lingard diteruskan Perreira dengan sundulan dari depan gawang untuk mengubah skor menjadi 3-1.</p>', 'ya', '386932.jpg'),
(11, 0, 45, 'De Gea Diharapkan Tampil Lawan Barcelona', '2015-07-22 18:43:47', '<p><strong>San Jose</strong> - David De Gea masih belum juga merumput pada laga-laga pramusim Manchester United. Kiper asal Spanyol itu masih dibekap cedera, namun diharapkan sudah bisa tampil pada laga melawan Barcelona akhir pekan ini.<br /><br />De Gea, yang sepanjang bursa transfer musim panas ini erat dikaitkan dengan Real Madrid, tetap dibawa ke tur pramusim (Setan Merah). Bersama dirinya, United juga membawa dua kiper lainnya, yakni Anders Lindegaard dan Sam Johnstone.<br /><br />Sebelum laga pramusim pertama melawan Club America, manajer United, Louis van Gaal, mengonfirmasi bahwa De Gea sedang cedera. Oleh karenanya, dia berencana memainkan Johnstone dan Lindegaard pada masing-masing babak ketika menghadapi Club America.<br /><br />Pada laga melawan San Jose Earthquakes, Rabu (22/7) pagi WIB, De Gea juga masih belum bisa tampil. Ditambah cederanya Lindegaard, Van Gaal pun memutuskan untuk memainkan Johnstone selama 90 menit.<br /><br />Van Gaal mengaku tidak ingin mengambil risiko terkait kondisi De Gea dan Lindegaard. Namun, dalam keterangan yang dilansir oleh United sendiri, Van Gaal mengharapkan De Gea sudah bisa tampil pada laga melawan Barcelona, Sabtu (25/7) waktu setempat, di Levis Stadium, Santa Clara, AS.<br /><br />Malam ini, dia masih cedera, ujar Van Gaal singkat.<br /><br />De Gea mengakhiri musim kemarin dengan tertatih meninggalkan lapangan. Ketika itu United tengah menghadapi Arsenal di Old Trafford.<br /><br />Cederanya De Gea membuat Victor Valdes ketiban berkah berupa debut dengan tim utama. Tenaga Valdes pun digunakan hingga laga terakhir musim kemarin ketika menghadapi Hull City.</p>', 'ya', '817993.jpg'),
(12, 0, 46, 'MU Harus Poles Pertahanan, tapi Akan Jadi Penantang Titel Premier League', '2015-07-31 14:03:58', '<p><strong>Manchester</strong> - Mantan penyerang Manchester United Lou Macari masih ragu dengan kualitas pertahanan yang dimiliki bekas timnya tersebut, walaupun tetap memprediksi skuat Louis van Gaal bakal jadi salah satu penantang serius dalam perebutan titel Premier League.<br /><br />Setelah mencatatkan tiga kemenangan dalam tur pramusimnya di Amerika Serikat, The Red Devils menelan kekalahan 0-2 ketika berhadapan dengan Paris Saint-Germain, Kamis (30/7/2015) pagi WIB kemarin, yang sekaligus menyudahi tur di negeri Paman Sam.<br /><br />Macari, yang membela MU periode 1973&ndash;1984, menyoroti secara kritis hasil tersebut. Menurutnya, di pertandingan lawan PSG itu (Setan Merah) sudah memperlihatkan kerentanan tersendiri di lini belakang.<br /><br />Kemarin malam cukup mengejutkan semua orang di kubu MU. Semua orang berpikir tim dalam kondisi bagus, tapi secara defensif mereka mengecewakan diri sendiri lawan PSG--mereka bertahan dengan buruk, (kata Macari kepada <em>Drivetime</em> yang dilansir<em> talkSPORT).</em><br /><br />Saya tetap berpikir mereka belum tahu pasangan-pasangan terbaik di lini belakang, mungkin seseorang akan datang sebelum musim bergulir, lanjutnya menduga.<br /><br />Di bursa musim panas sejauh ini Van Gaal sudah merekrut Matteo Darmian, Memphis Depay, Morgan Schneiderlin, Bastian Schweinsteiger, dan Sergio Romero. Macari percaya amunisi baru itu bisa membantu MU lebih dekat ke gelar <em>Premier League</em>, walaupun mengaku masih cemas dengan situasi terkait masa depan kiper David De Gea.<br /><br />Saya memperkirakan mereka akan jadi pesaing dalam perebutan gelar juara,mereka adalah Manchester United dan mereka sudah memoles tim. Musim lalu United tidak bagus, saya pikir mereka takkan mengulangi kesalahan seperti yang mereka lakukan di awal musim lalu, kalau mereka menjalani bulan pertama dengan buruk, alarm akan berdering kencang, tapi saya pikir mereka akan tampil bagus, ujarnya.<br /><br />Namun, satu-satunya hal yang belum membuat saya yakin adalah mengenai situasi kiper. Saya akan selalu menginginkan kiper top di tim saya, tapi itulah pertanyaan besarnya. Apakah ia (De Gea) akan bertahan atau pergi, ucap Macari.</p>', 'ya', '340179.jpg'),
(15, 0, 49, 'MU Sambut Musim Kompetisi dengan Amat Pede', '2015-08-02 16:09:48', '<p>Manchester United menyambut Premier League 2015/2016 dengan amat percaya diri. Makin solidnya para pemain lawas dan tambahan amunisi baru menjadi bekal Setan Merah.<br /><br />MU menambah skuat dengan mendatangkan pemain-pemain baru pada transfer musim panas 2015. Mereka, Memphis Depay, Bastian Schweinsteiger dan Morgan Schneiderlin.<br /><br />Sejauh ini di laga pramusim, MU membukukan hasil lumayan dalam tur di Amerika Serikat dalam International Champions Cup. Tiga dari empat laga yang dijalani berakhir dengan kemenangan, termasuk atas Barcelona. MU hanya mengantongi satu kekalahan dari dari Paris Saint-Germain.<br /><br />Dari laga-laga pramusim itu, Louis van Gaal memang belum benar-benar gembira dengan hasilnya. Tapi, masih ada waktu untuk mendongkrak performa tim.<br /><br />Gelandang MU, Daley Blind, yakin MU bisa tampil kompetitif di kompetisi nanti.<br /><br />(Kami amat percaya diri. Kami memiliki skuat yang bagus dan kami juga menyelesaikan pertandingan-pertandingan di pramusim dengan hasil yang sip), kata Blind seperti dikutip<em> Sports Mole. </em><br /><br />(Kalau melihat para pemain baru yang didatangkan ke tim ini pada musim ini, mereka akan menambah kualitas di lapangan. Aku rasa tim seperti Manchester United harus selalu tampil di lapangan untuk sebuah harga), jelas dia.</p>', 'ya', '915313.jpg'),
(11, 0, 50, 'Van Gaal Akan Beri Chicharito Kesempatan', '2015-08-02 21:55:34', '<p>Manajer Manchester United, Louis van Gaal, memberikan sinyal bahwa dirinya akan memberikan kesempatan kepada Javier Hernandez. Chicharito diminta bisa membuktikan diri tampil tajam.<br /><br />Chicharito yang tak masuk dalam rencana Van Gaal musim lalu, dipinjamkan ke Real Madrid. Bersama <em>El Real</em>, dia tampil sebanyak 23 kali di Liga Spanyol dengan kontribusi tujuh gol.<br /><br />Meski tampil cukup baik, Madrid tak mempunyai niat untuk mempertahankan Chicharito. Dia pun harus kembali ke Old Trafford di akhir musim 2014/2015.<br /><br />Di saat bersamaan, MU malah melepas dua strikernya. Radamel Falcao balik ke AS Monaco setelah masa pijamannya selesai. Sementara itu, Robin van Persie dilepas ke Fenerbahce.<br /><br />Van Gaal yang butuh pemain depan, lantas memberikan sinyal untuk memberi kesempatan kepada penyerang asal Meksiko itu.<br /><br />Javier bisa membuktikan dirinya lagi, dan sekarang (Radamel) Falcao dan (Robin) Van Persie sudah pergi. Kesempatan untuknya lebih baik, tutur Van Gaal seperti dilansir oleh <em>Daily Mail</em>.</p>', 'ya', '280883.jpg'),
(0, 1, 51, 'Depay: Rooney Membimbingku pada Laga Debut di MU', '2015-08-02 22:07:44', '<p>Memphis Depay sudah melakoni laga pertama bersama Manchester United. Dia mengaku mendapatkan banyak bimbingan dari Wayne Rooney disaat melakoni debut bersama <em>The Red Devils</em>.<br /><br />Depay diberi kepercayaan menjadi starter saat MU berhadapan dengan klub asal Meksiko, America. Dalam laga ujicoba yang menjadi bagian ajang International Champions Cup 2015 itu, MU menang 1-0 berkat gol Morgan Schneiderlin.<br /><br />Dalam pertandingan di CenturyLink Field, Seattle, Sabtu (18/7/2015), Depay bermain selama 45 menit. <em>Soccernet</em> mencatat dia cuma melakukan sekali sepakan ke gawang dan itu tak tepat sasaran.<br /><br />Selama satu babak bermain, Depay menempati posisi di belakang striker, yang ditempati oleh Rooney. Bagi Rooney, posisi itu tidaklah asing. Dia kerap memainkan peran itu di musim lalu.<br /><br />Wayne banyak melatihku sepanjang pertandingan dan memberitahuku mengenai posisi yang harus aku ambil saat bertahan, kata Depay di situs resmi MU.<br /><br />Kami harus tampil sebagai <em>duo</em> dan aku merasa cocok dengan dia. Sungguh merupakan suatu kebanggaan bisa bermain dengan pemain legenda seperti Wazza, imbuhnya.</p>', 'ya', '352203.jpeg'),
(11, 0, 52, 'MU Bakal Move On Jika De Gea Pergi', '2015-08-03 01:20:49', '<p>Peter Schmeichel menyebut David De Gea sebagai kiper fantastis yang mengalami banyak perkembangan di Manchester United. Tapi kalaupun dia harus hengkang, <em>The Red Devils</em> bisa mengatasi kepergiannya.<br /><br />Sudah sepanjang bursa transfer musim panas ini De Gea dirumorkan akan menuju Madrid. Meski pada awalnya pihak MU dan Louis van Gaal terus menyatakan kalau kiper utamanya itu tidak dijual, namun perkembangannya kini menjadi sulit diprediksi lantaran sang manajer menyebut kalau situasi terkait De Gea tidaklah menguntungkan.<br /><br />Maka rumor De Gea akan benar-benar angkat kaki dari Old Trafford pun berembus makin kuat. Dengan De Gea belum mau memperpanjang kontraknya yang habis di musim panas 2016, MU bisa jadi akhirnya akan mempertimbangkan melepasnya ke Madrid tahun ini daripada kehilangannya dengan cuma-cuma.<br /><br />Perginya De Gea akan jadi kehilangan besar untuk MU. Namun itu bukan berarti bencana karena MU diyakini akan bisa mendapatkan pengganti yang sepadan.<br /><br />David De Gea telah melakukan pekerjaan yang fantastis sejak dia datang ke klub. Dia dapat banyak kritik karena dianggap tak cukup bagus di enam bulan pertamanya di Old Trafford. Tapi masih di musim yang sama, Anda tahu kalau dia sudah menjadikan dirinya sebagai kiper fantastis. Dan di akhir musim tak ada yang berpikir dia (cukup baik), dia berkembang dengan fantastis, ucap Schmeichel.<br /><br />Tapi ini adalah Manchester United, mereka sudah memiliki banyak pemain besar, pemain-pemain terbaik dunia datang ke klub, mereka melakukan pekerjaan yang baik, mereka bisa <em>move on</em>. MU juga bisa <em>move on</em>, itulah yang dilakukan MU, lanjutnya di Skysport.<br /><br />De Gea tampil dalam 37 penampilan di musim 2014/2015 dan mencatatkan 11 clean sheet. Dia rata-rata kemasukan 0,97 gol di setiap pertandingan, serta rata-rata membuat 2,11 penyelamatan setiap 90 menit.</p>', 'ya', '507110.jpg'),
(11, 0, 53, 'Carrick Tegaskan Kesiapan MU untuk Jalani Laga Pembuka Musim Lawan Spurs', '2015-08-03 01:30:49', '<p>Gelandang Manchester United Michael Carrick menegaskan kesiapan timnya untuk menghadapi Tottenham Hotspur dalam pertandingan di awal musim Premier League 2015-16.<br /><br />MU baru saja menyelesaikan tur pramusimnya di Amerika Serikat. Dari empat pertandingan yang dijalani, tiga diakhiri dengan kemenangan sedangkan satu lainnya berupa kekalahan.<br /><br />Kemenangan-kemenangan tersebut diraih secara beruntun sejak laga pertama, yakni 1-0 atas Club America, 3-1 dari San Jose Earthquake, dan 3-1 lainnya atas Barcelona.<br /><br />Kekalahan tunggal dialami pada laga terakhir, Kamis (30/7/2015) pagi WIB. MU kalah 0-2 dari Paris Saint-Germain.<br /><br />Pun demikian Carrick menyatakan bahwa kekalahan tersebut tidak mengurangi kesiapan The Red Devils menghadapi musim baru, secara khusus untuk menghadapi Spurs tanggal 8 Agustus mendatang.<br /><br />Yang terpenting adalah pertandingan menghadapi Tottenham. Itulah yang terus kami katakan sejak kami datang ke sini. Itulah saat semuanya benar-benar dimulai, kata Carrick kepada MUTV dan dikutip <em>Sports Mole.</em><br /><br />Di sini kami sudah memainkan sejumlah pertandingan bagus untuk membantu kesiapan kami. Jika pertandingannya terlalu mudah, tentu ini takkan cukup.<br /><br />Jadi pertandingan lawan Barcelona dan laga kali ini sudah bagus buat kami, dan kami bisa memanfaatkannya untuk menyongsong partai lawan Tottenham nanti, tutur pesepakbola yang juga pernah membela Spurs periode 2004&ndash;2006 tersebut.</p>', 'ya', '636169.jpg'),
(0, 1, 54, 'Rooney dituntut untuk bisa membawa mu menjuarai premier league musim ini', '2015-08-04 19:13:07', '<p>roy keane menuntut rooney dapat membawa mu menjuarai liga dengan pengalamannya</p>', 'ya', '102752.jpg'),
(0, 1, 57, 'coba judul', '2015-08-05 11:10:11', '<p>coba text editor</p>', 'ya', '473876.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id_member` int(5) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `nama_member` varchar(30) NOT NULL,
  `jk_member` enum('L','P') DEFAULT NULL,
  `email_member` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tanggal` datetime NOT NULL,
  PRIMARY KEY (`id_member`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `user_name`, `nama_member`, `jk_member`, `email_member`, `password`, `tanggal`) VALUES
(2, 'tes1', 'tes1', 'P', 'tes1@mail.com', 'tes1', '0000-00-00 00:00:00'),
(13, 'irfan', 'irfanristia', 'L', 'irfan931226@gmail.com', '11a710a9ea67c5e268d18d23a911db78', '2015-07-01 22:29:06'),
(12, 'yogo', 'Yogo Dwi', 'L', 'yogodwi@gmail.com', '8fc9ea4323c75d30cd28d1ca854d56d8', '2015-06-27 14:58:38'),
(14, 'javier hernandez', 'Hernandez', 'L', 'hernandez14@gmail.com', 'a2a96704d21232d23aa4dd76ee33dc5b', '2015-07-01 23:14:30'),
(11, 'nanda', 'frenanda', 'L', 'ndafrog@gmail.com', '5d7b71ee3d50f942a21700f5c4db94ff', '2015-06-27 14:46:59'),
(15, 'bani', 'bani eko', 'L', 'bani12311@gmail.com', 'c8fb13328cd24a59bfa510a7f70d754b', '2015-08-02 14:47:16');

-- --------------------------------------------------------

--
-- Table structure for table `pemain`
--

CREATE TABLE IF NOT EXISTS `pemain` (
  `id_pemain` int(5) NOT NULL AUTO_INCREMENT,
  `nama_pemain` varchar(30) NOT NULL,
  `level_pemain` enum('pemain','pelatih') NOT NULL,
  `posisi` varchar(20) NOT NULL,
  `tempat_lahir_pemain` varchar(20) NOT NULL,
  `kewarganegaraan_pemain` varchar(20) NOT NULL,
  `tgl_lahir_pemain` date NOT NULL,
  `foto_pemain` varchar(50) NOT NULL,
  `kemunculan` int(4) NOT NULL,
  `jumlah_gol` int(4) NOT NULL,
  `tgl_bergabung` date DEFAULT NULL,
  `debut` varchar(50) DEFAULT NULL,
  `karir_sepakbola` text NOT NULL,
  PRIMARY KEY (`id_pemain`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `pemain`
--

INSERT INTO `pemain` (`id_pemain`, `nama_pemain`, `level_pemain`, `posisi`, `tempat_lahir_pemain`, `kewarganegaraan_pemain`, `tgl_lahir_pemain`, `foto_pemain`, `kemunculan`, `jumlah_gol`, `tgl_bergabung`, `debut`, `karir_sepakbola`) VALUES
(24, 'Luke Paul Hoare Shaw', 'pemain', 'Bek Sayap Kiri', 'Kingston upon Thames', 'INGGRIS', '1995-07-12', '308074.jpg', 75, 0, '2014-04-12', '27 September 2014 vs Liverpool', 'SOUTHAMPTON,MANCHESTER UNITED'),
(23, 'Rafael Pereira da Silva', 'pemain', 'Bek Sayap Kanan', 'Petropolis, Rio de J', 'BRAZIL', '1990-07-09', '205505.jpg', 175, 8, '2007-01-06', '03 Juni 2007 vs Chelsea', ' FLUMINENSE,MANCHESTER UNITED'),
(22, 'David De Gea', 'pemain', 'Kiper', 'Madrid,Spanyol', 'SPANYOL', '1990-11-07', '289062.jpg', 100, 0, '2011-06-03', '07 Agustus 2011 vs Man.City', 'ATLETICO MADRID,MANCHESTER UNITED'),
(19, 'Luis Van Gaal', 'pelatih', 'Manajer,Pelatih', 'Amsterdam', 'Belanda', '1990-12-20', '602142.jpg', 100, 0, '2014-01-23', '07 januari vs man city', 'Bayern Munchen,MU'),
(21, 'Ryan Joseph Giggs', 'pelatih', 'Asisten Manajer', 'Cardiff, Wales', 'Wales', '1973-11-29', '496185.jpg', 70, 0, '1987-11-14', '16 September 1991 vs Man.City', 'Manchester United'),
(25, 'Philip Anthony "Phil" Jones', 'pemain', 'Bek Tengah', ' Preston, United Kin', 'INGGRIS', '1992-02-21', '886993.jpg', 95, 7, '2011-06-13', '02 Desember 2011 vs Chicago Fire', 'BLACKBURN ROVERS,MANCHESTER UNITED'),
(26, 'Faustino Marcos Alberto Rojo', 'pemain', 'Bek Tengah', 'La Plata, Argentina', 'ARGENTINA', '1990-03-20', '543395.jpg', 64, 5, '2014-08-19', '14 September 2014 vs  Queens Park Rangers', 'ESTUDIANTES LP,SPARTAK MOSCOW,SPORTING CP,MANCHESTER UNITED'),
(27, 'Jonathan Grant "Jonny" Evans', 'pemain', 'Bek Tengah', 'Belfast, Northern Ir', 'IRLANDIA', '1988-01-03', '138153.jpg', 68, 11, '2006-07-13', '26 September 2007', 'MANCHESTER UNITED'),
(28, 'Angel Fabian Di Maria Hernande', 'pemain', 'Sayap Kiri,Gelandang', 'Rosario, Argentina', 'ARGENTINA', '1988-02-14', '76049.jpg', 14, 4, '2014-08-26', '30 Agustus 2014 vs Burnley', 'ROSARIO CENTRAL,BENFICA,REAL MADRID,MANCHESTER UNITED ');

-- --------------------------------------------------------

--
-- Table structure for table `pertandingan`
--

CREATE TABLE IF NOT EXISTS `pertandingan` (
  `id_pertandingan` int(5) NOT NULL AUTO_INCREMENT,
  `tanggal_pertandingan` date NOT NULL,
  `waktu_pertandingan` varchar(15) NOT NULL,
  `lawan_pertandingan` varchar(20) NOT NULL,
  `hasil_pertandingan` varchar(20) DEFAULT NULL,
  `pencetak_gol` varchar(20) DEFAULT NULL,
  `main` enum('kandang','tandang') NOT NULL,
  PRIMARY KEY (`id_pertandingan`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `pertandingan`
--

INSERT INTO `pertandingan` (`id_pertandingan`, `tanggal_pertandingan`, `waktu_pertandingan`, `lawan_pertandingan`, `hasil_pertandingan`, `pencetak_gol`, `main`) VALUES
(11, '2015-08-29', '22:00 - 23:50', 'Swansea City', '', '', 'tandang'),
(9, '2015-08-15', '22:00 - 23:50', 'Aston Villa', '', '', 'tandang'),
(10, '2015-08-22', '22:00 - 23:50', 'Newcastle United', '', '', 'kandang'),
(8, '2015-08-08', '22:00 - 23:50', 'Tottenham Hotspurs', '', '', 'kandang'),
(12, '2015-09-12', '22:00 - 23:50', 'Liverpool', '', '', 'kandang'),
(13, '2015-09-19', '22:00 - 23:50', 'Southampton', '', '', 'tandang'),
(14, '2015-09-26', '22:00 - 23:50', 'Sunderland', '', '', 'kandang'),
(15, '2015-10-03', '22:00 - 23:50', 'Arsenal', '', '', 'tandang'),
(16, '2015-10-17', '22:00 - 23:50', 'Everton', '', '', 'tandang'),
(17, '2015-10-24', '22:00 - 23:50', 'Manchester City', '', '', 'kandang'),
(18, '2015-10-31', '22:00 - 23:50', 'Crystal Palace', '', '', 'tandang'),
(19, '2015-11-07', '22:00 - 23:50', 'West Brom', '', '', 'kandang'),
(20, '2015-11-21', '22:00 - 23:50', 'Watford', '', '', 'tandang'),
(21, '2015-11-28', '22:00 - 23:50', 'Leicester City', '', '', 'tandang'),
(22, '2015-12-05', '22:00 - 23:50', 'West Ham United', '', '', 'kandang'),
(23, '2015-12-12', '22:00 - 23:50', 'AFC Bournemouth', '', '', 'tandang'),
(24, '2015-12-19', '22:00 - 23:50', 'Norwich City', '', '', 'kandang'),
(25, '2015-12-26', '22:00 - 23:50', 'Stoke City', '', '', 'tandang'),
(26, '2015-12-28', '22:00 - 23:50', 'Chelsea', '', '', 'kandang');
