<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Portal Berita Manchaster United</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">     
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>
	<header id="header">
		<div class="navbar navbar-inverse set-radius-zero">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="logo pull-left"><a href="index.php"><img src="gambar/images/home/UI.png" width="200px" height="100px" alt="" /></a></div>
					</div>
					<div class="col-sm-9">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<div class="col-sm-3">
									<div class="search_box pull-right">
										<form action="pencarian.php" method="get">
											<input type="text" name="cari" placeholder="Pencarian"/>
										</form>
									</div>
								</div>								
								<li><a href="login.php"><i class="fa fa-lock fa-1x"></i><font color="#fff">Login</font></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section id="slider">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
						</ol>						
						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-6">
									<h1><span>Manchester</span>United</h1>
								  	<p>Portal Berita Manchester United</p>									
								</div>
								<div class="col-sm-6"><img src="gambar/images/home/UI.png" class="girl img-responsive" alt="" /></div>
						  </div>
							<div class="item">
								<div class="col-sm-6">
									<h1><span>Manchester</span>United</h1>
								  	<p>Portal Berita Manchester United</p>	
								</div>
								<div class="col-sm-6">
									<img src="gambar/images/home/UI.png" class="girl img-responsive" alt="" />
								</div>
							</div>							
						</div>						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<a href="index.php"><h2>Beranda</h2></a>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="unggah_berita.php">Unggah Berita</a></h4>
										</div>
									</div>
								</div>
							</div> 
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="profil_pemain.php">Profil Pelatih & Pemain</a></h4>
										</div>
									</div>
								</div>
							</div>							
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="sejarah_manchaster_united.php">Sejarah Manchaster United</a></h4>
										</div>
									</div>
								</div>
							</div> 
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="stadion.php">Stadion</a></h4>
										</div>
									</div>
								</div>
							</div> 
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="jadwal_pertandingan.php">Jadwal Pertandingan</a></h4>
										</div>
									</div>
								</div>
							</div> 
						</div><!--/category-products-->
						<a href="index.php"><h2>Arsip Berita</h2></a>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<?php
							include "inc/koneksi.php";
							$query = "SELECT DISTINCT date_format(tanggal_berita, '%M %Y') as bulantahun FROM berita";
							$hasil = mysql_query($query);
							echo "<ul>";
							while ($data = mysql_fetch_array($hasil))
							{
							    echo "<li><a href='viewarsip.php?blnth=".$data['bulantahun']."'>".$data['bulantahun']."</a></li>";
							}
							echo "</ul>";

							?>
						</div>
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items">
						<h2 class="title text-center">ARSIP BERITA</h2>
							<!-- Content left -->
							<div class="content-box-right">
								<div class="content-box-right-in">
									<?php
									include "inc/koneksi.php";
									$blnth = $_GET['blnth']; // membaca bulan dan tahun dari parameter link

									// query SQL untuk menampilkan artikel berdasarkan bulan dan tahun
									// yang diambil dari parameter link

									$query = "SELECT * FROM berita WHERE date_format(tanggal_berita, '%M %Y') = '$blnth'";
									$hasil = mysql_query($query);
									while ($data = mysql_fetch_array($hasil)){
										$konten = substr($data['isi_berita'],0,400);
									?>
									<a href="baca_berita.php?id_berita=<?php echo $data['id_berita']; ?>">
									<h2><?php echo $data['judul_berita']; ?></h2>
									</a>
									<p class="text-content"><?php echo $konten; ?></p>
									<br />
									<a href="baca_berita.php?id_berita=<?php echo $data['id_berita']; ?>">Baca</a>
									<?php } ?>
								</div>
							</div>
							<!-- Content left end -->			
						</div>
					</div>					
				</div>
			</div>
		</div>
	</section>	
	<footer id="footer"><!--Footer-->
		<div class="footer-widget" class="center">
			<div class="container">
				<div class="row">
					<div class="col-sm-15">
						<div class="single-widget">
						<center>
							<h2>KONTAK ADMIN</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Telpon 	: 08994355146</a></li>
								<li><a href="#">Email	: irfan931226@gmail.com</a></li>
								<li><a href="#">BBM		: 54F0EA54</a></li>
							</ul>
							</center>
						</div>
					</div>                  					
				</div>
			</div>
		</div>		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">				
					<marquee><p class="pull-left">Designed by: IRFAN RISTIA RAHMADANI</p></marquee>
				</div>
			</div>
		</div>
	</footer>  
	</footer><!--/Footer-->
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>