<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Portal Berita Manchaster United</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">     
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>
	<header id="header">
		<div class="navbar navbar-inverse set-radius-zero">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="logo pull-left"><a href="index.php"><img src="gambar/images/home/UI.png" width="200px" height="100px" alt="" /></a></div>
					</div>
					<div class="col-sm-9">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<div class="col-sm-3">
									<div class="search_box pull-right">
										<form action="pencarian.php" method="get">
											<input type="text" name="cari" placeholder="Pencarian"/>
										</form>
									</div>
								</div>								
								<li><a href="login.php"><i class="fa fa-lock fa-1x"></i><font color="#fff">Login</font></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section id="slider">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
						</ol>						
						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-6">
									<h1><span>Manchester</span>United</h1>
								  	<p>Portal Berita Manchester United</p>									
								</div>
								<div class="col-sm-6"><img src="gambar/images/home/UI.png" class="girl img-responsive" alt="" /></div>
						  </div>
							<div class="item">
								<div class="col-sm-6">
									<h1><span>Manchester</span>United</h1>
								  	<p>Portal Berita Manchester United</p>	
								</div>
								<div class="col-sm-6">
									<img src="gambar/images/home/UI.png" class="girl img-responsive" alt="" />
								</div>
							</div>							
						</div>						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<a href="index.php"><h2>Beranda</h2></a>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="unggah_berita.php">Unggah Berita</a></h4>
										</div>
									</div>
								</div>
							</div> 
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="profil_pemain.php">Profil Pelatih & Pemain</a></h4>
										</div>
									</div>
								</div>
							</div>							
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="sejarah_manchaster_united.php">Sejarah Manchaster United</a></h4>
										</div>
									</div>
								</div>
							</div> 
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="stadion.php">Stadion</a></h4>
										</div>
									</div>
								</div>
							</div> 
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel panel-default">
										<div class="panel-heading">
										<h4 class="panel-title"><a href="jadwal_pertandingan.php">Jadwal Pertandingan</a></h4>
										</div>
									</div>
								</div>
							</div> 
						</div><!--/category-products-->						
					<a href="index.php"><h2>Arsip Berita</h2></a>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<?php
							include "inc/koneksi.php";	
							$query = "SELECT DISTINCT date_format(tanggal_berita, '%M %Y') as bulantahun FROM berita";
							$hasil = mysql_query($query);
							echo "<ul>";
							while ($data = mysql_fetch_array($hasil))
							{
							    echo "<li><a href='viewarsip.php?blnth=".$data['bulantahun']."'>".$data['bulantahun']."</a></li>";
							}
							echo "</ul>";
							?>
						</div>
					</div>
				</div>				
					<div class="category-tab"><!--category-tab-->
						<div class="tab-content">
							<div class="tab-pane fade active in" id="tshirt" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="gambar/pemain/old2.jpg" alt="" />
												<div class="col-sm-9 padding-right">
												</div>												
											</div>									
										</div>
									</div>
								</div>
								<h3> Old trafford ("The Theatre of Dreams") </h3><br>
Manchester United merupakan klub yang sarat akan sejarah. 
Penggemar klub yang bermarkas di kota Manchester ini juga semakin hari semakin bertambah banyak jumlahnya.
Dengan segala permasalahan dan prestasi yang diraih,tak heran klub MU merupakan salah satu klub dengan pesona yang paling menarik dan memiliki history yang sangat bervariasi.
Klub ini dibentuk dengan nama Newton Heath Lancashire and Yorkshire Railwaiy F.C (Newton Heath LYR F.C.) pada tahun 1878  oleh para pekerja rel kereta api di Newton Heath.
History klub MU, Pada waktu itu, kaos tim berwarna hijau emas dan mereka bermain di lapangan kecil di North Road, dekat stasiun kereta api Piccadilly Manchester selama lima belas tahun, sebelum pindah ke Bank Street pada 1893. 
Klub telah mengikuti kompetisi sepak bola tahun sebelumnya dan mulai memutuskan hubungannya dengan stasiun kereta api, sehingga menjadi perusahaan mandiri, dan mengangkat seorang sekretaris  dan pada akhirnya membuang nama LYR dari nama mereka untuk menjadi Newton Heath F.C saja. 
Namun, pada tahun 1902, tim nyaris bangkrut  dengan utang lebih dari 2500 dan lapangan Bank Street mereka pun telah ditutup. 
Sebelum klub mereka bubar, mereka menerima sokongan investasi dari J.H. Davies, direktur Manchester Breweries. 
Ketika itu diadakan rapat untuk mengganti nama perkumpulan. 
Manchester Celtic dan Manchester Central adalah nama yang diusulkan pada awalnya. 
Namun, nama akhirnya ditetapkan dan Manchester United secara resmi eksis mulai 26 April 1902. 
Pada waktu itu pula Davies memutuskan untuk mengganti warna tim dan terpilihlah warna merah dan putih sebagai warna tim Manchester United.
Ernest Mangnall dipilih menjadi sekretaris klub menggantikan James West yang mengundurkan diri tanggal 28 September 1902. 
Mangnall berusaha untuk mengangkat tim ke Divisi Satu pada waktu itu namun mengalami kegagalan pada upaya pertamanya, dan hanya menempati urutan 5 Liga Divisi Dua. 
Mangnall pin memutuskan untuk menambah beberapa pemain ke dalam klub, seperti Harry Moger, Dick Duckworth, dan John Picken, dan Charlie Roberts yang waktu itu membuat perubahan yang cukup signifikan. 
Pada waktu itu klub berada di posisi tiga klasmen akhir musim 1903 1904. Mereka selanjutnya promosi ke Divisi Satu setelah finis urutan kedua Divisi Dua musim 1905 1906. 
Musim pertama klub di Divisi satu berakhir kurang baik, dan hanya menempati urutan 8 klasemen. 
Namun, mereka akhirnya memenangkan gelar liga pertamanya pada tahun 1908. 
Pada waktu itu, rival mereka, Manchester City, sedang diselidiki karena menggaji pemain diatas regulasi yang ditetapkan FA dan Man City didenda 250euro serta 18 pemain mereka dihukum tidak boleh bermain untuk mereka lagi. 
MU dengan cepat mengambil kesempatan dari situasi ini, mereka merekrut Billy Meredith dan Sandy Turnbull, dll. 
Namun pemain baru ini tidak boleh bermain dahulu sebelum tahun Baru 1907, akibat dari skors dari FA dan mereka mulai bermain pada musim 1907 1908. 
Klub kembali memenangkan trofi Liga Divisi Satu untuk kedua kalinya pada musim 1910 1911.<br>
					
					<div class="category-tab"><!--category-tab-->
						<div class="tab-content">
							<div class="tab-pane fade active in" id="tshirt" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="gambar/pemain/mun.jpg" alt="" />
												<div class="col-sm-9 padding-right">
												</div>												
											</div>									
										</div>
									</div>
								</div>
								<h3>Tragedi Kecelakaan di German</h3> 
United pindah ke lapangan barunya Old Trafford.
Mereka memainkan pertandingan pertamanya di stadion baru tersebut pada tanggal 19 Februari 1910 melawan Liverpool, tetapi mereka kalah 4 3. 
Mereka puasa trofi lagi sejak musim 1911 1912, dan mereka tidak didukung oleh Mangnall lagi karena dia pindah ke Manchester City setelah 10 tahunnya bersama United. 
Mereka 41 tahun bermain tanpa memenangkan satu trofi pun. 
United terdegradasi pada tahun 1922 setelah sepuluh tahun bermain di Divisi Satu. 
Mereka naik divisi lagi tahun 1925, tetapi kesulitan untuk masuk jajaran papan atas liga Divisi Satu dan mereka turun divisi lagi pada tahun 1931. 
United meraih mencapaian terendah sepanjang sejarahnya yaitu posisi 20 klasemen Divisi Dua 1934. 
Kekuatan mereka kembali ketika musim 1938 1939.
Pada tahun 1945, Matt Busby dipilih menjadi manajer Manchester United. 
Dia meminta sesuatu yang tidak lazim pada pekerjaannya, seperti menunujuk tim sendiri, memilih pemain yang akan direkrut sendiri, dan menentukan jadwal latihan para pemain sendiri. 
Dia sendiri pada waktu itu telah kehilangan lowongan manager di klub lain, Liverpool F.C, karena pekerjaan yang diinginkannya itu menurut petinggi Liverpool adalah pekerjaan seorang direktur. 
Waktu itu, United memberikan kesempatan untuk ide inovatifnya. 
Pertama, Busby tidak merekrut pemain, tetapi seorang asisten manager yang bernama Jimmy Murphy. 
Keputusan tersebut merupakan keputusan yang sangat tepat. 
Busby membayar kepercayaan dengan mengantar MU ke posisi kedua liga pada tahun 1947, 1948, and 1949 serta memenangkan Piala FA tahun 1948. 
Stan Pearson, Jack Rowley, Allenby Chilton, dan Charlie Mitten memiliki andil yang besar dalam pencapaian United ini. 
Mereka kembali meraih gelar Divisi Satu pada 1952. 
Busby memasukkan beberapa pemain muda seperti Roger Byrne, Bill Foulkes, Mark Jones dan Dennis Viollet. 
Namun, mereka membutuhkan waktu untuk menunjukkan permainan terbaik mereka, akibatnya United tergelincir ke posisi 8 pada 1953. 
Tetapi tim kembali memenangkan liga tahun 1956 dengan tim yang usia rata-rata pemainnya hanya 22 tahun, mencetak 103 gol. 
Kebijakan tentang pemain muda ini mengantarkannya menjadi salah satu manager yang paling sukses menangani Manchester United (pertengahan 1950an, pertengahan akhir 1960an dan 1990an). 
Busby memiliki pemain talenta tinggi yang bernama Duncan Edwards dan memainkan debutnya pada umur 16 tahun di 1953. 
Edwards dikatakan dapat bermain disegala posisi dan banyak yang melihatnya bermain mengatakan bahwa dia adalah pemain terbaik. 
Musim berikutnya, 1956 1957, mereka menang liga kembali dan mencapai final Piala FA, namun kalah dari Aston Villa. 
Mereka menjadi tim Inggris pertama yang ikut serta dalam kompetisi Piala Champions Eropa, atas kebijakan FA. 
United dapat mencapai babak semifinal dan kemudian dikandaskan Real Madrid. 
Dalam perjalanannya ke semi-final, United juga mencatatkan kemenangan yang tetap menunjukkan bahwa mereka adalah tim besar, ketika mengalahkan tim juara Belgia Anderlecht 10 0 di Maine Road.
Tragedi terjadi pada musim berikutnya, ketika pesawat yang membawa tim pulang dari pertandingan Piala Champions Eropa mengalami kecelakaan saat mendarat di Munich, Jerman untuk mengisi bahan bakar. 
Tragedi Munich air tanggal 6 Februari 1958 tersebut telah merenggut nyawa 8 pemain tim Geoff Bent, Roger Byrne, Eddie Colman, Duncan Edwards, Mark Jones, David Pegg, Tommy Taylor dan Liam Whelan dan 15 penumpang lainnya, termasuk sebagian staf United, Walter Crickmer, Bert Whalley dan Tom Curry. 
Ketika itu, terdapat rumor bahwa tim akan mengundurkan diri dari kompetisi, namun Jimmy Murphy mengambil alih posisi manager ketika Busby dirawat di rumah sakit, ban klub tetap melanjutkan kompetisinya. 
Meski kehilangan beberapa pemain, mereka mencapai final Piala FA 1958, namun mereka kalah dari Bolton Wanderers. 
Akhir musim, UEFA menawarkan FA untuk dapat mengirimkan MU dan juara liga Wolverhampton Wanderers untuk berpartisipasi di Piala Champions untuk penghargaan kepada para korban kecelakaan, namun FA menolak. 
United menekan Wolves pada musim berikutnya dan menyelesaikan liga di posisi kedua klasemen, tidak buruk untuk sebuah tim yang kehilangan sembilan pemain akibat tragedi kecelakaan di jerman. 

					<div class="category-tab"><!--category-tab-->
						<div class="tab-content">
							<div class="tab-pane fade active in" id="tshirt" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="gambar/pemain/matt_busby.jpg" alt="" />
												<div class="col-sm-9 padding-right">
												</div>												
											</div>									
										</div>
									</div>
								</div>
								<h3>Sir Matt Busby</h3>
Busby membangun kembali tim di tahun 60an, dan membeli beberapa pemain seperti Denis Law dan Pat Crerand. 
Mungkin pemain yang paling terkenal dari sejumlah pemain muda ini adalah George Best. 
Tim memenangkan Piala FA tahun 1963, walaupun saat itu tim hanya finis diurutan 19 Divisi Satu. 
Keberhasilan di Piala FA membuat pemain menjadi termotivasi dan membuat klub terangkat pada posisi kedua liga tahun 1964, dan kembali memenangkan liga di tahun 1965 dan 1967. 
United memenangkan Piala Champions Eropa 1968, mengalahkan tim asuhan Eusebio SL Benfica 4 1 dipertandingan final, menjadi tim Inggis pertama yang memenagkan kompetisi ini. 
Tim MU saat itu memiliki Pemain Terbaik Eropa, yaitu: Bobby Charlton, Denis Law and George Best. 
Matt Busby mengundurkan diri pada tahun 1969 dan digantikan oleh pelatih tim cadangan, Wilf McGuinness.
Setelah masa yang sukses, United kemudian mengalami masa sulit ketika ditangani Wilf McGuinness, dan berada diurutan delapan liga pada musim 1969 1970. 
Kemudian dia mengawali musim 1970 1971 dengan buruk, sehingga McGuinness kembali turun jabatan menjadi pelatih tim cadangan. 
Busby kembali melatih United, walaupun hanya 6 bulan. 
Dibawah asuhan Busby, United mendapat hasil yang lebih baik, namun pada akhirnya ia meninggalkan klub pada tahun 1971. 
Dalam waktu itu, United kehilangan beberapa pemain kuncinya seperti Nobby Stiles dan Pat Crerand. 
Kemudian, Frank O Farrell ditunjuk sebagai suksesor Busby. 
Seperti McGuinness, O Farrell tidak bertahan lebih dari 18 bulan. 
Tommy Docherty menjadi manager di akhir 1972. 
Docherty menyelamatkan United dari degradasi namun United terdegradasi pada 1974, yang saat itu trio Best, Law and Charlton telah meninggalkan klub. 
Denis Law pindah ke Manchester City pada musim panas tahun 1973. 
Pemain seperti Lou Macari, Stewart Houston dan Brian Greenhoff direkrut untuk menggantikan Best, Law and Charlton, namun tidak menghasilkan apa apa. 
Tim meraih promosi pada tahun pertamanya di Divisi Dua, dengan peran besar pemain muda berbakat Steve Coppell yang bermain baik pada musim pertamanya bersama United, bergabung dari Tranmere Rovers. 
United mencapai Final Piala FA tahun 1976, tetapi mereka dikalahkan Southampton. 
Mereka mencapai final lagi tahun 1977 dan mengalahkan Liverpool 2 1. Didalam kesuksesan ini, Docherty dipecat karena diketahui memiliki hubungan dengan istri fisioterapi.
						</div>
					</div>
					</div><!--/category-tab-->
				</div>
			</div>
		</div>
	</div>
</div>
	</section>
<footer id="footer"><!--Footer-->
		<div class="footer-widget" class="center">
			<div class="container">
				<div class="row">
					<div class="col-sm-15">
						<div class="single-widget">
						<center>
							<h2>KONTAK ADMIN</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Telpon 	: 08994355146</a></li>
								<li><a href="#">Email	: irfan931226@gmail.com</a></li>
								<li><a href="#">BBM		: 54F0EA54</a></li>
							</ul>
							</center>
						</div>
					</div>                  					
				</div>
			</div>
		</div>		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">				
					<marquee><p class="pull-left">Designed by: IRFAN RISTIA RAHMADANI</p></marquee>
				</div>
			</div>
		</div>
	</footer>  
	</footer><!--/Footer-->
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>